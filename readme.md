# Startup'UTT

Startup'UTT est un site web statique incluant les technologies AJAX.

## Accès

Il suffit de cliquer sur la page index.html pour accéder au site.

```bash
index.html
```

## Utilisation

```
Parcourez toutes les pages, surtout la page "quizz" pour vous tester!
```

## Contribution
Le projet a été fait dans un cadre académique à l'Université de Technologie de Troyes.
## License
[UTT](https://www.utt.fr/)